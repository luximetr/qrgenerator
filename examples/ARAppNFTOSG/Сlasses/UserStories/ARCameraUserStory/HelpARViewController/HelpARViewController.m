//
//  HelpARViewController.m
//  QRGenerator
//
//  Created by admin on 03.04.17.
//
//

#import "HelpARViewController.h"

@interface HelpARViewController () <UITableViewDataSource>

@property (nonatomic, strong) NSArray <NSString *> *availableModels;

@end

@implementation HelpARViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.availableModels = @[@"airplane", @"earth", @"stark", @"superman", @"tank"];
}

#pragma mark - IBActions

- (IBAction)actionTapOnButtonBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.availableModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = self.availableModels[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
