//
//  PreviewViewControllerProtocol.h
//  QRGenerator
//
//  Created by admin on 28.03.17.
//
//

#import <Foundation/Foundation.h>

@protocol PreviewViewControllerProtocol <NSObject>

- (void)configureWithImage:(UIImage *)image stringValue:(NSString *)stringValue;

@end
