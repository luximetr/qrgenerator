//
//  PreviewViewController.h
//  QRGenerator
//
//  Created by admin on 28.03.17.
//
//

#import <UIKit/UIKit.h>
#import "PreviewViewControllerProtocol.h"

@interface PreviewViewController : UIViewController <PreviewViewControllerProtocol>

@end
