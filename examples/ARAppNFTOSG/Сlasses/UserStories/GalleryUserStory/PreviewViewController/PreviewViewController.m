//
//  PreviewViewController.m
//  QRGenerator
//
//  Created by admin on 28.03.17.
//
//

#import "PreviewViewController.h"
#import "VKSdk.h"
#import "QRGeneratorGradient.h"

@interface PreviewViewController ()

@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) IBOutlet UILabel *valueLabel;
@property (nonatomic, strong) UIImage *currentImage;
@property (nonatomic, copy) NSString *stringValue;

@end

@implementation PreviewViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureNavigationBar];
    self.imageView.image = self.currentImage;
    self.valueLabel.text = self.stringValue;
    [self.view.layer insertSublayer:[QRGeneratorGradient gradientWithFrame:self.view.bounds] atIndex:0];
}

#pragma mark - Private

- (void)configureNavigationBar {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

#pragma mark - PreviewViewControllerProtocol

- (void)configureWithImage:(UIImage *)image stringValue:(NSString *)stringValue {
    self.currentImage = image;
    self.stringValue = stringValue;
}

#pragma mark - IBActions

- (IBAction)actionTapOnCloseButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionTapOnShareButton:(id)sender {
    VKShareDialogController *shareDialog = [VKShareDialogController new];
    shareDialog.text = self.stringValue;
    shareDialog.uploadImages = @[[VKUploadImage uploadImageWithImage:self.imageView.image andParams:[VKImageParameters jpegImageWithQuality:1.f]]];
    __weak typeof(self) weakSelf = self;
    [shareDialog setCompletionHandler:^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }];
    [self presentViewController:shareDialog animated:YES completion:nil];
}

@end
