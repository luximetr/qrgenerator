//
//  GalleryViewController.m
//  QRGenerator
//
//  Created by admin on 24.03.17.
//
//

#import "GalleryViewController.h"
#import "GalleryCollectionViewCell.h"
#import "StoryboardConstants.h"
#import "PreviewViewControllerProtocol.h"
#import "QRGeneratorGradient.h"

static const int kPhotoNameIndex = 0;
static const int kPhotoImageIndex = 1;

@interface GalleryViewController () <UICollectionViewDataSource>

@property (retain, nonatomic) IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UILabel *emptyGalleryLabel;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *deleteButton;
@property (nonatomic, strong) NSMutableArray <NSArray *> *galleryDataSourceArray;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, assign) BOOL isDeleteMode;

@end

@implementation GalleryViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([GalleryCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:GalleryCollectionViewCellIdentifier];
    
    [self reloadCollection];
    
    [self.view.layer insertSublayer:[QRGeneratorGradient gradientWithFrame:self.view.bounds] atIndex:0];
    self.emptyGalleryLabel.hidden = self.galleryDataSourceArray.count != 0;
    self.isDeleteMode = NO;
}

- (void)reloadCollection {
    self.galleryDataSourceArray = [[NSMutableArray alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    
    NSString *path = [paths firstObject];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
    for (int i = 0; i < [directoryContent count]; i++) {
        NSString *fileName = directoryContent[i];
        NSData *dataFromFile = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", path, fileName]];
        UIImage *image = [UIImage imageWithData:dataFromFile];
        if (image) {
            [self.galleryDataSourceArray addObject:@[[[fileName componentsSeparatedByString:@"."] firstObject], image]];
        }
    }
    [self.collectionView reloadData];
}

- (void)removeImage:(NSString *)filename
{
    filename = [filename stringByAppendingString:@".png"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Image was removed" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:^{
            [self reloadCollection];
        }];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.galleryDataSourceArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GalleryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:GalleryCollectionViewCellIdentifier forIndexPath:indexPath];
    [cell configureWithImage:self.galleryDataSourceArray[indexPath.row][kPhotoImageIndex]
                 valueString:self.galleryDataSourceArray[indexPath.row][kPhotoNameIndex]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    if (!self.isDeleteMode) {
        [self performSegueWithIdentifier:PresentPreviewViewControllerSegueIdentifier sender:self];
    }
    else {
        __weak typeof (self) weakSelf = self;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Do you want to delete?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            weakSelf.isDeleteMode = NO;
            [weakSelf removeImage:self.galleryDataSourceArray[indexPath.row][kPhotoNameIndex]];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:okAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:PresentPreviewViewControllerSegueIdentifier]) {
        UINavigationController *navigationController = segue.destinationViewController;
        id <PreviewViewControllerProtocol> destinationVC = [navigationController.viewControllers firstObject];
        [destinationVC configureWithImage:self.galleryDataSourceArray[self.selectedIndexPath.row][kPhotoImageIndex] stringValue:self.galleryDataSourceArray[self.selectedIndexPath.row][kPhotoNameIndex]];
    }
}

#pragma mark - IBActions

- (IBAction)actionTapOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionTapOnDeleteButton:(id)sender {
    self.isDeleteMode = !self.isDeleteMode;
    self.deleteButton.tintColor = self.isDeleteMode ? [UIColor redColor] : [UIColor whiteColor];
}

@end
