//
//  EditViewControllerProtocol.h
//  QRGenerator
//
//  Created by admin on 27.03.17.
//
//

#import <Foundation/Foundation.h>

static const int EditBottomColorIndex = 0;
static const int EditTopColorIndex = 1;
static const int EditBackgroundColorIndex = 2;

@protocol EditViewControllerProtocol <NSObject>

@property (nonatomic, copy) NSString *dataString;
@property (nonatomic, strong) UIImage *centerImage;
@property (nonatomic, strong) NSMutableArray *colorsArray;

@end
