//
//  EditViewController.h
//  QRGenerator
//
//  Created by admin on 24.03.17.
//
//

#import <UIKit/UIKit.h>
#import "EditViewControllerProtocol.h"

@interface EditViewController : UIViewController <EditViewControllerProtocol>

@end
