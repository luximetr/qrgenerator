//
//  ColorPickerViewController.m
//  QRGenerator
//
//  Created by admin on 27.03.17.
//
//

#import "ColorPickerViewController.h"
#import "StoryboardConstants.h"
#import "HRColorPickerView.h"
#import "HRColorMapView.h"
#import "HRBrightnessSlider.h"

@interface ColorPickerViewController ()

@property (nonatomic, strong) HRColorPickerView *colorPickerView;

@end

@implementation ColorPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.colorPickerView = [[HRColorPickerView alloc] init];
    self.colorPickerView.color = [UIColor greenColor];
    self.selectedColor = self.colorPickerView.color;
    
    HRColorMapView *colorMapView = [[HRColorMapView alloc] init];
    colorMapView.saturationUpperLimit = @1;
    colorMapView.tileSize = @1;
    [self.colorPickerView addSubview:colorMapView];
    self.colorPickerView.colorMapView = colorMapView;
    
    HRBrightnessSlider *slider = [[HRBrightnessSlider alloc] init];
    slider.brightnessLowerLimit = @0;
    [self.colorPickerView addSubview:slider];
    self.colorPickerView.brightnessSlider = slider;
    
    [self.colorPickerView addTarget:self
                             action:@selector(colorWasChanged:)
                   forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.colorPickerView];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.colorPickerView.frame = (CGRect) {.origin = CGPointZero, .size = self.view.frame.size};
    
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        CGRect frame = self.colorPickerView.frame;
        frame.origin.y = self.topLayoutGuide.length;
        frame.size.height -= self.topLayoutGuide.length;
        self.colorPickerView.frame = frame;
    }
}

- (void)colorWasChanged:(HRColorPickerView *)view {
    self.selectedColor = view.color;
}

#pragma mark - IBActions

- (IBAction)actionTapOnBackButton:(id)sender {
    self.selectedColor = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionTapOnDoneButton:(id)sender {
    [self performSegueWithIdentifier:UnwindFromImagePickerSegueIdentifier sender:self];
}

@end
