//
//  ColorPickerViewController.h
//  QRGenerator
//
//  Created by admin on 27.03.17.
//
//

#import <UIKit/UIKit.h>

@interface ColorPickerViewController : UIViewController

@property (nonatomic, strong) UIColor *selectedColor;

@end
