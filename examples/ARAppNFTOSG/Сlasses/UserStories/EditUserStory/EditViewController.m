//
//  EditViewController.m
//  QRGenerator
//
//  Created by admin on 24.03.17.
//
//

#import "EditViewController.h"
#import "StoryboardConstants.h"
#import "DCQRCode-Swift.h"
@import DCQRCode;
#import "ColorPickerViewController.h"
#import "QRGeneratorGradient.h"

static const float kQRSize = 200.f;
static const float kButtonsCornerRadius = 4.f;
static const float kButtonBorderWidth = 1.f;

@interface EditViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *qrImageView;
@property (retain, nonatomic) IBOutlet UIButton *bottomColorButton;
@property (retain, nonatomic) IBOutlet UIButton *topColorButton;
@property (retain, nonatomic) IBOutlet UIButton *backgroundColorButton;
@property (retain, nonatomic) IBOutlet UIButton *centerImageButton;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *bottomColorButtonLeadingConstraint;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthConstraint;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *betweenButtonConstraint;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *between2and3ButtonConstraint;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *between3and4ButtonConstraint;

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, strong) UIImagePickerController *imagePicker;

@end

@implementation EditViewController
@synthesize dataString;
@synthesize colorsArray;
@synthesize centerImage;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureSelfView];
    [self reloadView];
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    [self.view.layer insertSublayer:[QRGeneratorGradient gradientWithFrame:self.view.bounds] atIndex:0];
}

#pragma mark - Private

- (void)configureSelfView {
    self.bottomColorButton.layer.cornerRadius = kButtonsCornerRadius;
    self.topColorButton.layer.cornerRadius = kButtonsCornerRadius;
    self.backgroundColorButton.layer.cornerRadius = kButtonsCornerRadius;
    self.centerImageButton.layer.cornerRadius = kButtonsCornerRadius;
    
    self.bottomColorButton.layer.borderWidth = kButtonBorderWidth;
    self.topColorButton.layer.borderWidth = kButtonBorderWidth;
    self.backgroundColorButton.layer.borderWidth = kButtonBorderWidth;
    self.centerImageButton.layer.borderWidth = kButtonBorderWidth;
    
    self.bottomColorButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.topColorButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.backgroundColorButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.centerImageButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.qrImageView.layer.borderWidth = 1.f;
    self.qrImageView.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.centerImageButton.clipsToBounds = YES;
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    self.betweenButtonConstraint.constant = self.between2and3ButtonConstraint.constant = self.between3and4ButtonConstraint.constant = (screenWidth - (self.bottomColorButtonLeadingConstraint.constant * 2) - (self.buttonWidthConstraint.constant * 4)) / 3.f;
}

- (void)reloadView {
    DCQRCode *code = [[DCQRCode alloc] initWithInfo:self.dataString size:CGSizeMake(kQRSize, kQRSize)];
    code.bottomColor = self.colorsArray[EditBottomColorIndex];
    code.topColor = self.colorsArray[EditTopColorIndex];
    code.quietZoneColor = self.colorsArray[EditBackgroundColorIndex];
    code.topImage = self.centerImage;
    self.qrImageView.image = [code generateQRCode];
    
    self.bottomColorButton.backgroundColor = self.colorsArray[EditBottomColorIndex];
    self.topColorButton.backgroundColor = self.colorsArray[EditTopColorIndex];
    self.backgroundColorButton.backgroundColor = self.colorsArray[EditBackgroundColorIndex];
    [self.centerImageButton setBackgroundImage:self.centerImage forState:UIControlStateNormal];
    self.centerImageButton.backgroundColor = [UIColor whiteColor];
}

#pragma mark - IBActions

- (IBAction)actionTapOnBottomSelectColor:(id)sender {
    self.selectedIndex = EditBottomColorIndex;
    [self performSegueWithIdentifier:ShowColorPickerSegueIdentifier sender:self];
}
- (IBAction)actionTapOnTopSelectColor:(id)sender {
    self.selectedIndex = EditTopColorIndex;
    self.centerImage = nil;
    [self performSegueWithIdentifier:ShowColorPickerSegueIdentifier sender:self];
}
- (IBAction)actionTapOnBackgroundSelectColor:(id)sender {
    self.selectedIndex = EditBackgroundColorIndex;
    [self performSegueWithIdentifier:ShowColorPickerSegueIdentifier sender:self];
}

- (IBAction)actionTapOnCenterImageSelect:(id)sender {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.allowsEditing = YES;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)unwindToEditViewController:(UIStoryboardSegue *)segue {
    if ([segue.identifier isEqualToString:UnwindFromImagePickerSegueIdentifier]) {
        ColorPickerViewController *sourceVC = segue.sourceViewController;
        self.colorsArray[self.selectedIndex] = sourceVC.selectedColor;
    }
    [self reloadView];
}

- (IBAction)actionTapOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionTapOnConfirmButton:(id)sender {
    [self performSegueWithIdentifier:UnwindFromEditViewControllerSegueIdentifier sender:self];
}

- (IBAction)actionTapOnResetButton:(id)sender {
    self.colorsArray = [@[[UIColor whiteColor], [UIColor blackColor], [UIColor whiteColor]] mutableCopy];
    self.centerImage = nil;
    [self reloadView];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    self.centerImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self reloadView];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
