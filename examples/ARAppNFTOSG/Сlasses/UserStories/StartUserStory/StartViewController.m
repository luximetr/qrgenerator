//
//  StartViewController.m
//  QRGenerator
//
//  Created by admin on 22.03.17.
//
//

#import "StartViewController.h"
#import "DCQRCode-Swift.h"
#import "DynamicButton-Swift.h"
@import DCQRCode;
@import DynamicButton;
#import "YALContextMenuTableView.h"
#import "ContextMenuCell.h"
#import "StoryboardConstants.h"
#import "EditViewControllerProtocol.h"
#import <VK_ios_sdk/VKSdk.h>
#import "UIImageView+BlurAnimation.h"
#import "QRGeneratorGradient.h"

static NSString *const menuCellIdentifier = @"rotationCell";
static const float kQRSize = 200.f;
static const int kMenuItemNameIndex = 0;
static const int kMenuItemIconIndex = 1;
static const int kMaxNumberCount = 25;

typedef NS_ENUM(NSInteger, MenuItem) {
    MenuItemClose = 0,
    MenuItemGallery,
    MenuItemAR,
    MenuItemShare,
    MenuItemSave
};

@interface StartViewController () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, YALContextMenuTableViewDelegate, UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField *inputTextField;
@property (retain, nonatomic) IBOutlet UIImageView *qrImageView;
@property (retain, nonatomic) IBOutlet DynamicButton *createButton;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *inputTextFieldBottomConstraint;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *navigationRightButton;
@property (retain, nonatomic) IBOutlet UILabel *currentLettersCountLabel;
@property (retain, nonatomic) IBOutlet UIView *saveInformView;
@property (retain, nonatomic) IBOutlet UIView *copiedToClipboardInformView;

@property (nonatomic, strong) YALContextMenuTableView *contextMenuTableView;
@property (nonatomic, strong) NSMutableArray *menuItems;

@property (nonatomic, assign) CGFloat textFieldInitBottomConstraint;
@property (nonatomic, strong) UIColor *bottomColor;
@property (nonatomic, strong) UIColor *topColor;
@property (nonatomic, strong) UIColor *quietZoneColor;
@property (nonatomic, strong) UIImage *centerImage;
@property (nonatomic, strong) NSString *currentCodeValue;

@end

@implementation StartViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bottomColor = [UIColor whiteColor];
    self.topColor = [UIColor blackColor];
    self.quietZoneColor = [UIColor whiteColor];
    
    [self updateQRCode];
    self.inputTextField.delegate = self;
    self.currentLettersCountLabel.text = [NSString stringWithFormat:@"0/%i", kMaxNumberCount];
    
    self.createButton.tintColor = [UIColor grayColor];
    [self configureNavigationBar];
    [self configureMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.textFieldInitBottomConstraint = self.inputTextFieldBottomConstraint.constant;
    
    UILongPressGestureRecognizer *longPressOnImage = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressOnImage:)];
    longPressOnImage.minimumPressDuration = 1.f;
    [self.qrImageView addGestureRecognizer:longPressOnImage];
    
    self.saveInformView.alpha = self.copiedToClipboardInformView.alpha = 0.f;
    self.saveInformView.layer.cornerRadius = self.copiedToClipboardInformView.layer.cornerRadius = 4.f;
    
    [self.view.layer insertSublayer:[QRGeneratorGradient gradientWithFrame:self.view.bounds] atIndex:0];
    self.currentCodeValue = @"";
}

#pragma mark - UIKeyboardNotifications

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.inputTextFieldBottomConstraint.constant = self.textFieldInitBottomConstraint + keyboardSize.height;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.inputTextFieldBottomConstraint.constant = self.textFieldInitBottomConstraint;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Private

- (void)configureNavigationBar {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)configureMenu {
    self.menuItems = [[NSMutableArray alloc] init];
    self.menuItems[MenuItemClose] = @[@"", @"icn_close"];
    self.menuItems[MenuItemGallery] = @[@"Gallery", @"icn_gallery"];
    self.menuItems[MenuItemAR] = @[@"AR", @"icn_ar"];
    self.menuItems[MenuItemShare] = @[@"Share", @"icn_share"];
    self.menuItems[MenuItemSave] = @[@"Save", @"icn_save"];
}

- (void)updateQRCode {
    self.qrImageView.image = [self qrCodeWithInfo:self.inputTextField.text];
}

- (UIImage *)qrCodeWithInfo:(NSString *)info {
    DCQRCode *code = [[DCQRCode alloc] initWithInfo:info size:CGSizeMake(kQRSize, kQRSize)];
    code.topColor = self.topColor;
    code.bottomColor = self.bottomColor;
    code.quietZoneColor = self.quietZoneColor;
    code.topImage = self.centerImage;
    return [code generateQRCode];
}

- (void)saveCurrentQRCode {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *newFileName = [NSString stringWithFormat:@"%@%@", self.self.currentCodeValue, @".png"];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:newFileName];
    NSData* data = UIImagePNGRepresentation(self.qrImageView.image);
    [data writeToFile:path atomically:YES];

    [self showSaveInformPopup];
}

- (void)sharePictureInVk {
    [self updateQRCode];
    VKShareDialogController *shareDialog = [VKShareDialogController new];
    shareDialog.text = self.inputTextField.text;
    shareDialog.uploadImages = @[[VKUploadImage uploadImageWithImage:[self qrCodeWithInfo:self.inputTextField.text] andParams:[VKImageParameters jpegImageWithQuality:1.f]]];
    __weak typeof(self) weakSelf = self;
    [shareDialog setCompletionHandler:^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }];
    [self presentViewController:shareDialog animated:YES completion:nil];
}

- (void)showSaveInformPopup {
    [self blurImageView:self.qrImageView withInformView:self.saveInformView];
}

- (void)showCopiedToClipboardInformPopup {
    [self blurImageView:self.qrImageView withInformView:self.copiedToClipboardInformView];
}

- (void)longPressOnImage:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [UIPasteboard generalPasteboard].image = self.qrImageView.image;
        [self showCopiedToClipboardInformPopup];
    }
}

- (void)blurImageView:(UIImageView *)imageView withInformView:(UIView *)informView {
    [UIView animateWithDuration:0.3f animations:^{
        informView.alpha = 1.f;
    }];
    
    UIImageView *bluredImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
    bluredImageView.image = imageView.image;
    bluredImageView.blurTintColor = [UIColor colorWithWhite:0.4f alpha:0.5f];
    bluredImageView.blurRadius = 20;
    bluredImageView.animationRepeatCount = 1;
    bluredImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.qrImageView addSubview:bluredImageView];
    
    [self.qrImageView addConstraints:@[[NSLayoutConstraint constraintWithItem:bluredImageView
                                 attribute:NSLayoutAttributeLeading
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.qrImageView
                                 attribute:NSLayoutAttributeLeading
                                multiplier:1.f constant:0.f],
                                       
    [NSLayoutConstraint constraintWithItem:bluredImageView
                                 attribute:NSLayoutAttributeTop
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.qrImageView
                                 attribute:NSLayoutAttributeTop
                                multiplier:1.f constant:0.f],
                                       
    [NSLayoutConstraint constraintWithItem:bluredImageView
                                 attribute:NSLayoutAttributeTrailing
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.qrImageView
                                 attribute:NSLayoutAttributeTrailing
                                multiplier:1.f constant:0.f],
                                       
    [NSLayoutConstraint constraintWithItem:bluredImageView
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.qrImageView
                                 attribute:NSLayoutAttributeBottom
                                multiplier:1.f constant:0.f]]];
    
    __weak typeof(bluredImageView) weakBluredView = bluredImageView;
    [bluredImageView ty_blurInAnimationWithDuration:0.3f completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakBluredView ty_blurOutAnimationWithDuration:0.3f];
            [UIView animateWithDuration:0.3f animations:^{
                informView.alpha = 0.f;
            } completion:^(BOOL finished) {
                [weakBluredView removeFromSuperview];
            }];
        });
    }];
}

#pragma mark - UITextFieldDelegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.createButton.tintColor = [UIColor whiteColor];
    if (newString.length <= kMaxNumberCount) {
        self.currentLettersCountLabel.text = [NSString stringWithFormat:@"%i/%i", (int)newString.length, kMaxNumberCount];
        return YES;
    }
    else {
        return NO;
    }
}

#pragma mark - IBActions

- (IBAction)actionTapOnDoneButton:(id)sender {
    [self updateQRCode];
    self.createButton.tintColor = [UIColor grayColor];
    self.currentCodeValue = self.inputTextField.text;
}

- (IBAction)actionTapOnEditButton:(id)sender {
    [self performSegueWithIdentifier:ShowEditViewControllerSegueIdentifier sender:self];
}

- (IBAction)actionTapOnMenuButton:(id)sender {
    [self.inputTextField resignFirstResponder];
    if (!self.contextMenuTableView) {
        self.contextMenuTableView = [[YALContextMenuTableView alloc]initWithTableViewDelegateDataSource:self];
        self.contextMenuTableView.animationDuration = 0.2f / (CGFloat)self.menuItems.count;
        //optional - implement custom YALContextMenuTableView custom protocol
        self.contextMenuTableView.yalDelegate = self;
        //optional - implement menu items layout
        self.contextMenuTableView.menuItemsSide = Left;
        self.contextMenuTableView.menuItemsAppearanceDirection = FromTopToBottom;
        
        //register nib
        UINib *cellNib = [UINib nibWithNibName:@"ContextMenuCell" bundle:nil];
        [self.contextMenuTableView registerNib:cellNib forCellReuseIdentifier:menuCellIdentifier];
    }
    
    // it is better to use this method only for proper animation
    [self.contextMenuTableView showInView:self.navigationController.view withEdgeInsets:UIEdgeInsetsMake([UIApplication sharedApplication].statusBarFrame.size.height, 0, 0, 0) animated:YES];
}

- (IBAction)actionTapOnBackgroundView:(id)sender {
    [self.inputTextField resignFirstResponder];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:ShowEditViewControllerSegueIdentifier]) {
        id<EditViewControllerProtocol>destinationVC = segue.destinationViewController;
        destinationVC.dataString = self.inputTextField.text;
        destinationVC.colorsArray = [@[self.bottomColor, self.topColor, self.quietZoneColor] mutableCopy];
        destinationVC.centerImage = self.centerImage;
        
        [self updateQRCode];
        self.createButton.tintColor = [UIColor grayColor];
    }
}

- (IBAction)unwindToStartViewController:(UIStoryboardSegue *)segue {
    id <EditViewControllerProtocol> sourceVC = segue.sourceViewController;
    
    self.bottomColor = sourceVC.colorsArray[EditBottomColorIndex];
    self.topColor = sourceVC.colorsArray[EditTopColorIndex];
    self.quietZoneColor = sourceVC.colorsArray[EditBackgroundColorIndex];
    self.centerImage = sourceVC.centerImage;
    
    [self updateQRCode];
}

#pragma mark - YALContextMenuTableView DataSource and Delegate

- (void)tableView:(YALContextMenuTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView dismisWithIndexPath:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContextMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:menuCellIdentifier forIndexPath:indexPath];
    
    if (cell) {
        cell.backgroundColor = [UIColor clearColor];
        cell.menuTitleLabel.text = self.menuItems[indexPath.row][kMenuItemNameIndex];
        cell.menuImageView.image = [UIImage imageNamed:self.menuItems[indexPath.row][kMenuItemIconIndex]];
    }
    
    return cell;
}

#pragma mark - YALContextMenuTableViewDelegate

- (void)contextMenuTableView:(YALContextMenuTableView *)contextMenuTableView didDismissWithIndexPath:(NSIndexPath *)indexPath {
   
    switch (indexPath.row) {
        case MenuItemClose:
            break;
            
        case MenuItemGallery:
            [self performSegueWithIdentifier:ShowGalleryViewControllerSegueIdentifier sender:self];
            break;
            
        case MenuItemAR:
            [self performSegueWithIdentifier:ShowARViewControllerSegueIdentifier sender:self];
            break;
            
        case MenuItemShare:
            [self sharePictureInVk];
            break;
            
        case MenuItemSave:
            [self saveCurrentQRCode];
            break;
            
        default:
            break;
    }
}

@end
