//
//  Constants.m
//  QRGenerator
//
//  Created by admin on 24.03.17.
//
//

#import "StoryboardConstants.h"

#pragma mark - Segue Identifiers

NSString * const ShowEditViewControllerSegueIdentifier = @"ShowEditViewControllerSegueIdentifier";
NSString * const ShowARViewControllerSegueIdentifier = @"ShowARViewControllerSegueIdentifier";
NSString * const ShowGalleryViewControllerSegueIdentifier = @"ShowGalleryViewControllerSegueIdentifier";
NSString * const ShowColorPickerSegueIdentifier = @"ShowColorPickerSegueIdentifier";
NSString * const UnwindFromImagePickerSegueIdentifier = @"UnwindFromImagePickerSegueIdentifier";
NSString * const UnwindFromEditViewControllerSegueIdentifier = @"UnwindFromEditViewControllerSegueIdentifier";
NSString * const PresentPreviewViewControllerSegueIdentifier = @"PresentPreviewViewControllerSegueIdentifier";
NSString * const ShowHelpARViewControllerSugueIdentifier = @"ShowHelpARViewControllerSugueIdentifier";

#pragma mark - Cell Identifiers

NSString * const GalleryCollectionViewCellIdentifier = @"GalleryCollectionViewCellIdentifier";
