//
//  Constants.h
//  QRGenerator
//
//  Created by admin on 24.03.17.
//
//

#import <UIKit/UIKit.h>

#pragma mark - Segue Identifiers

extern NSString * const ShowEditViewControllerSegueIdentifier;
extern NSString * const ShowARViewControllerSegueIdentifier;
extern NSString * const ShowGalleryViewControllerSegueIdentifier;
extern NSString * const ShowColorPickerSegueIdentifier;
extern NSString * const UnwindFromImagePickerSegueIdentifier;
extern NSString * const UnwindFromEditViewControllerSegueIdentifier;
extern NSString * const PresentPreviewViewControllerSegueIdentifier;
extern NSString * const ShowHelpARViewControllerSugueIdentifier;

#pragma mark - Cell Identifiers

extern NSString * const GalleryCollectionViewCellIdentifier;
