//
//  QRGeneratorGradient.m
//  QRGenerator
//
//  Created by admin on 03.04.17.
//
//

#import "QRGeneratorGradient.h"

@implementation QRGeneratorGradient

+ (CAGradientLayer *)gradientWithFrame:(CGRect)frame {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = frame;
    gradient.colors = @[(id)[UIColor colorWithRed:0.29 green:0.10 blue:0.29 alpha:1.00].CGColor, (id)[UIColor colorWithRed:0.40 green:0.16 blue:0.36 alpha:1.00].CGColor];
    return gradient;
}

@end
