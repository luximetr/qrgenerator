//
//  GalleryCollectionViewCell.h
//  QRGenerator
//
//  Created by admin on 27.03.17.
//
//

#import <UIKit/UIKit.h>

@interface GalleryCollectionViewCell : UICollectionViewCell

- (void)configureWithImage:(UIImage *)image valueString:(NSString *)valueString;

@end
