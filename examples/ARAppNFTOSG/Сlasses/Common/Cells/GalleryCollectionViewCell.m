//
//  GalleryCollectionViewCell.m
//  QRGenerator
//
//  Created by admin on 27.03.17.
//
//

#import "GalleryCollectionViewCell.h"

@interface GalleryCollectionViewCell ()

@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) IBOutlet UILabel *valueLabel;

@end

@implementation GalleryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 4.f;
    // Initialization code
}

- (void)configureWithImage:(UIImage *)image valueString:(NSString *)valueString {
    self.imageView.image = image;
    self.valueLabel.text = valueString;
}

@end
