//
//  QRGeneratorGradient.h
//  QRGenerator
//
//  Created by admin on 03.04.17.
//
//

#import <Foundation/Foundation.h>

@interface QRGeneratorGradient : NSObject

+ (CAGradientLayer *)gradientWithFrame:(CGRect)frame;

@end
