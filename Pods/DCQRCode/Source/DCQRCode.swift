//
//  DCQRCode.swift
//  Example_DCPathButton
//
//  Created by tang dixi on 8/5/2016.
//  Copyright © 2016 Tangdixi. All rights reserved.
//

import Foundation
import CoreImage
import AVFoundation
import UIKit

@objc public final class DCQRCode: NSObject {
  
  fileprivate lazy var version:Int = self.fetchQRCodeVersion()
  fileprivate var info:String
  fileprivate var size:CGSize
  
  public var bottomColor = UIColor.white
  public var topColor = UIColor.black
  public var bottomImage:UIImage?
  public var topImage:UIImage?
  public var quietZoneColor:UIColor = UIColor.white
  
  var positionInnerColor:UIColor?
  var positionOuterColor:UIColor?
  var positionInnerStyle:[(UIImage, DCQRCodePosition)]?
  var positionOuterStyle:[(UIImage, DCQRCodePosition)]?
  
  public var centerImage:UIImage?
  
  public init(info:String, size:CGSize) {
    self.info = info
    self.size = size.scale(UIScreen.main.scale)
  }
  
  public func generateQRCode() -> UIImage {
    
    /* Start from a white blank image */
    let originImage = CIImage.empty()
    var filter = generateQRCodeFilter(self.info) >>> resizeFilter(self.size) >>> falseColorFilter(topColor, color1: bottomColor)
    
    /* Processing through Core Image */
    if let topImage = self.topImage {
      
      guard let ciTopImage = CIImage(image: topImage) else { fatalError() }
      let topImageResizeFilter = resizeFilter(self.size)
      let resizeTopImage = topImageResizeFilter(ciTopImage)
      let alphaQRCode = generateAlphaQRCode()
      
      filter = filter >>> blendWithAlphaMaskFilter(resizeTopImage, maskImage: alphaQRCode)
      
    }
    
    if let bottomImage = self.bottomImage {
      
      guard let ciBottimImage = CIImage(image: bottomImage) else { fatalError() }
      let bottomResizeFilter = resizeFilter(self.size)
      let resizeBottimImage = bottomResizeFilter(ciBottimImage)
      let reverseAlphaQRCode = generateReverseAlphaQRCode()
      
      filter = filter >>> blendWithAlphaMaskFilter(resizeBottimImage, maskImage: reverseAlphaQRCode)
      
    }
    
    let ciImage = filter(originImage)
    var image = UIImage(ciImage: ciImage)
    
    /* Use Core Graphics to attach stuffs */
    
    UIGraphicsBeginImageContextWithOptions(image.size, false, 0)
    image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))

    defer {
      
      UIGraphicsEndImageContext()
      
    }
    
    if let positionOuterStyle = self.positionOuterStyle {
      
      self.clearOuterPosition(positionOuterStyle)
      
      positionOuterStyle.forEach {
        changeInnerPositionStyle($0, position: $1)
      }
      image = UIGraphicsGetImageFromCurrentImageContext()!
      
    }
    
    if let positionInnerStyle = self.positionInnerStyle {
      
      self.clearInnerPosition(positionInnerStyle)
      
      positionInnerStyle.forEach {
        changePositionInnerColor($0, position: $1)
      }
      image = UIGraphicsGetImageFromCurrentImageContext()!
      
    }
    
    if let positionOuterColor = self.positionOuterColor, self.positionOuterStyle == nil {
      
      changeOuterPositionColor(positionOuterColor, position: .bottomLeft)
      changeOuterPositionColor(positionOuterColor, position: .topLeft)
      changeOuterPositionColor(positionOuterColor, position: .topRight)
      
      image = UIGraphicsGetImageFromCurrentImageContext()!
      
    }
    
    if let positionInnerColor = self.positionInnerColor, self.positionInnerStyle == nil {
      
      let originColorImage = CIImage.empty()
      let color = CIColor(color: positionInnerColor)
      let filter = constantColorGenerateFilter(color) >>> cropFilter(CGRect(x: 0, y: 0, width: 2, height: 2))
      let ciColorImage = filter(originColorImage)
      let colorImage = UIImage(ciImage: ciColorImage)
      
      changePositionInnerColor(colorImage, position: .topLeft)
      changePositionInnerColor(colorImage, position: .topRight)
      changePositionInnerColor(colorImage, position: .bottomLeft)
      
      image = UIGraphicsGetImageFromCurrentImageContext()!
      
    }
    
    if let centerImage = self.centerImage {
      
      changePositionInnerColor(centerImage, position: .center)
      
      image = UIGraphicsGetImageFromCurrentImageContext()!
      
    }
    
    /* Make sure the QRCode's quiet zone clear */
    
    if self.bottomImage != nil || self.bottomColor != nil {
      
      changeOuterPositionColor(self.quietZoneColor, position: .quietZone)
      
      image = UIGraphicsGetImageFromCurrentImageContext()!
      
    }
    
    return image
    
  }
}

//MARK: Private Method -

extension DCQRCode {
  
  fileprivate func fetchQRCodeVersion() -> Int {
    
    /* Fetch the qrcode version */
    let originImage = CIImage.empty()
    let filter = generateQRCodeFilter(self.info)
    let width = Int(filter(originImage).extent.width)
    
    print("Version:\((width - 23)/4 + 1)")
    
    return (width - 23)/4 + 1
    
  }
  
  fileprivate func generateAlphaQRCode() -> CIImage {
    
    let originImage = CIImage.empty()
    let filter = generateQRCodeFilter(self.info) >>> resizeFilter(self.size) >>> falseColorFilter(UIColor.black, color1: UIColor.white) >>> maskToAlphaFilter()
    let image = filter(originImage)
    return image
    
  }
  
  fileprivate func generateReverseAlphaQRCode() -> CIImage {
    
    let originImage = CIImage.empty()
    let filter = generateQRCodeFilter(self.info) >>> resizeFilter(self.size) >>> falseColorFilter(UIColor.white, color1: UIColor.black) >>> maskToAlphaFilter()
    let image = filter(originImage)
    return image
    
  }
  
  fileprivate func changeOuterPositionColor(_ color:UIColor, position:DCQRCodePosition) {
    
    let path = position.outerPositionPath(self.size, version: self.version)
    color.setStroke()
    path.stroke()
    
  }
  
  fileprivate func changeInnerPositionStyle(_ image:UIImage, position:DCQRCodePosition) {
    
    let rect = position.outerPositionRect(self.size, version: self.version)
    
    image.draw(in: rect)
    
  }
  
  fileprivate func changePositionInnerColor(_ image:UIImage, position:DCQRCodePosition) {
    
    let rect = position.innerPositionRect(self.size, version: self.version)
    image.draw(in: rect)
    
  }
  
  fileprivate func clearOuterPosition(_ positionOuterStyle:[(UIImage, DCQRCodePosition)]) {
    
    guard let context = UIGraphicsGetCurrentContext() else { fatalError() }
    
    positionOuterStyle.forEach {
      
      let rect = $1.outerPositionRect(self.size, version: self.version)
      
      context.addRect(rect)
      
    }
  
    context.clip()
    context.clear(CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))

  }
  
  fileprivate func clearInnerPosition(_ positionInnerStyle:[(UIImage, DCQRCodePosition)]) {
    
    guard let context = UIGraphicsGetCurrentContext() else { fatalError() }
    
    positionInnerStyle.forEach {
      
      let rect = $1.innerPositionRect(self.size, version: self.version)
      
      context.addRect(rect)
      
    }
    
    context.clip()
    context.clear(CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
    
  }
}

//MARK: Position Stuff -

enum DCQRCodePosition {
  
  static let innerPositionTileOriginWidth = CGFloat(3)
  static let outerPositionPathOriginLength = CGFloat(6)
  static let outerPositionTileOriginWidth = CGFloat(7)
  
  case topLeft, topRight, bottomLeft, center, quietZone
  
  func innerPositionRect(_ size:CGSize, version:Int) -> CGRect {
    
    /* Caculate the size at first */
    let originTileCount = CGFloat((version - 1) * 4 + 23)
    var tileWidth = size.width * DCQRCodePosition.innerPositionTileOriginWidth / originTileCount
    
    let scaleRatio = size.width / originTileCount
    let originToEdge = 6.0 * scaleRatio
    let centerImageLength = 0.2 * size.width
    
    tileWidth += UIScreen.main.scale
    
    switch self {
      
      case .topLeft:
        let point = CGPoint(x: floor(DCQRCodePosition.innerPositionTileOriginWidth * scaleRatio), y: DCQRCodePosition.innerPositionTileOriginWidth * scaleRatio).toFloor()
        let rect = CGRect(origin: point, size: CGSize(width: tileWidth, height: tileWidth).toCeil())
        return rect
      case .topRight:
        let point = CGPoint(x: size.width - originToEdge, y: DCQRCodePosition.innerPositionTileOriginWidth * scaleRatio).toFloor()
        let rect = CGRect(origin: point, size: CGSize(width: tileWidth, height: tileWidth).toCeil())
        return rect
      case .bottomLeft:
        let point = CGPoint(x: DCQRCodePosition.innerPositionTileOriginWidth * scaleRatio, y: size.width - originToEdge).toFloor()
        let rect = CGRect(origin: point, size: CGSize(width: tileWidth, height: tileWidth).toCeil())
        return rect
      case .center:
        let point = CGPoint(x: size.width/2.0 - centerImageLength/2.0, y: size.width/2.0 - centerImageLength/2.0)
        let rect = CGRect(origin: point, size: CGSize(width: centerImageLength, height: centerImageLength))
        return rect
      default:
        return CGRect.zero
    }
    
  }
  
  func outerPositionRect(_ size:CGSize, version:Int) -> CGRect {
    
    let zonePathWidth = size.width / CGFloat((version - 1) * 4 + 23)
    let outerPositionWidth = zonePathWidth * DCQRCodePosition.outerPositionTileOriginWidth + UIScreen.main.scale
    var rect = CGRect(origin: CGPoint(x: zonePathWidth, y: zonePathWidth), size: CGSize(width: outerPositionWidth, height: outerPositionWidth))

    switch self {
    case .topLeft:
      
      return rect
      
    case .topRight:
      
      let offset = size.width - outerPositionWidth - zonePathWidth*2
      rect = rect.offsetBy(dx: offset, dy: 0)
      return rect
      
    case .bottomLeft:
      
      let offset = size.width - outerPositionWidth - zonePathWidth*2
      rect = rect.offsetBy(dx: 0, dy: offset)
      return rect

    default:
      
      return CGRect.zero
      
    }
    
  }
  
  func outerPositionPath(_ size:CGSize, version:Int) -> UIBezierPath {
    
    let zonePathWidth = size.width / CGFloat((version - 1) * 4 + 23)
    let positionFrameWidth = zonePathWidth * DCQRCodePosition.outerPositionPathOriginLength
    
    let topLeftPoint = CGPoint(x: zonePathWidth * 1.5, y: zonePathWidth * 1.5)
    var rect = CGRect(origin: topLeftPoint, size: CGSize(width: positionFrameWidth, height: positionFrameWidth))
    
    switch self {
      case .topLeft:
        
        let path = rect.rectPath()
        path.lineWidth = zonePathWidth + UIScreen.main.scale
        path.lineCapStyle = .square
        
        return path
      
      case .topRight:
        
        let offset = size.width - positionFrameWidth - topLeftPoint.x * 2
        rect = rect.offsetBy(dx: offset, dy: 0)
        let path = rect.rectPath()
        path.lineWidth = zonePathWidth + UIScreen.main.scale
        path.lineCapStyle = .square
        
        return path
      case .bottomLeft:
        
        let offset = size.width - positionFrameWidth - topLeftPoint.x * 2
        rect = rect.offsetBy(dx: 0, dy: offset)
        let path = rect.rectPath()
        path.lineWidth = zonePathWidth + UIScreen.main.scale
        path.lineCapStyle = .square
        
        return path
      case .quietZone:
      
        let zoneRect = CGRect(origin: CGPoint(x: zonePathWidth*0.5, y: zonePathWidth*0.5) , size: CGSize(width: size.width - zonePathWidth, height: size.width - zonePathWidth))
        let zonePath = zoneRect.rectPath()
        zonePath.lineWidth = zonePathWidth + UIScreen.main.scale
        zonePath.lineCapStyle = .square
      
        return zonePath
      default:
        
        return UIBezierPath()
    }
    
    
  }
  
}

//MARK: Some Convenience Caculation Extension -

extension CGPoint {
  
  func toFloor() -> CGPoint {
    
    return CGPoint(x: floor(self.x), y: floor(self.y))
    
  }
  func toCeil() -> CGPoint {
    
    return CGPoint(x: ceil(self.x), y: ceil(self.y))
    
  }
  
}

extension CGSize {
  
  func scale(_ ratio: CGFloat) -> CGSize {
    
    return CGSize(width: self.width * ratio, height: self.height * ratio)
    
  }
  
  func toFloor() -> CGSize {
    
    return CGSize(width: floor(self.width), height: floor(self.height))
    
  }
  func toCeil() -> CGSize {
    
    return CGSize(width: ceil(self.width), height: ceil(self.height))
    
  }
  
}

extension CGRect {
  
  func rectPath() -> UIBezierPath {
    
    let path = UIBezierPath()
    
    path.move(to: self.origin)
    path.addLine(to: CGPoint(x: self.origin.x, y: self.origin.y + self.size.height))
    path.addLine(to: CGPoint(x: self.origin.x + self.size.width, y: self.origin.y + self.size.height))
    path.addLine(to: CGPoint(x: self.origin.x + self.size.width, y: self.origin.y))
    path.addLine(to: self.origin)
    
    return path
    
  }
  
  
}



